var translations =
   {
      "et": [
         {
            "first_question"   :  "Miks me<br>jääme<br>haigeks?",
            "second_question"  :  "Geeniinfo<br>kasutamine<br>meditsiinis",
            "third_question"   :  "Milline on<br>Sinu<br>tervisetoit?",
            "watch_video"      :  "Vaata",
            "video_one"        :  "videos/K15-Miks-jaame-haigeks-EST.mp4",
            "video_two"        :  "videos/geeniinfo_kasutamine_meditsiinis_EST.mp4",
            "video_three"      :  "videos/tervisetoit_EST.mp4",
         }
      ],
      "en": [
         {
            "first_question"   :  "Why do we<br>fall<br>ill?",
            "second_question"  :  "Using<br>genetic information<br>in medicine",
            "third_question"   :  "What is<br>your<br>health food?",
            "watch_video"      :  "Play",
            "video_one"        :  "videos/K15-Miks-jaame-haigeks-ENG.mp4",
            "video_two"        :  "videos/geeniinfo_kasutamine_meditsiinis_EN.mp4",
            "video_three"      :  "videos/tervisetoit_ENG.mp4",
         }
      ],
      "ru": [
         {
            "first_question"   :  "Почему<br>мы<br>заболеваем?",
            "second_question"  :  "Использование<br>генетической информации<br>в медицине.",
            "third_question"   :  "Ваша<br>здоровая<br>пища",
            "watch_video"      :  "Смотреть",
            "video_one"        :  "videos/K15-Miks-jaame-haigeks-RUS.mp4",
            "video_two"        :  "videos/geeniinfo_kasutamine_meditsiinis_RU.mp4",
            "video_three"      :  "videos/tervisetoit_RUS.mp4",
         }
      ],
      "fi": [
         {
            "first_question"   :  "Miksi<br>me<br>sairastumme?",
            "second_question"  :  "Geenitiedon<br>käyttö<br>lääketieteessä",
            "third_question"   :  "Millainen on<br>Sinun terveellinen<br>ruokavaliosi?",
            "watch_video"      :  "Katso",
            "video_one"        :  "videos/K15-Miks-jaame-haigeks-FIN.mp4",
            "video_two"        :  "videos/geeniinfo_kasutamine_meditsiinis_EN.mp4",
            "video_three"      :  "videos/tervisetoit_FIN.mp4",
         }
      ]
   }